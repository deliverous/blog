IMAGE="deliverous/blog"
PNG = $(shell find content/ -type f -name '*.png')
JPEG_FROM_PNG = $(patsubst %.png, %.jpg, $(PNG))
JPEG_ORIGINAL = $(shell find content/ -type f -name '*.jpg' ! -name '*__thumbnail-*' )
JPEG_ORIGINAL += $(JPEG_FROM_PNG)
JPEG_SQUARE = $(patsubst %.jpg, %__thumbnail-square.jpg, $(JPEG_ORIGINAL))
JPEG_WIDE = $(patsubst %.jpg, %__thumbnail-wide.jpg, $(JPEG_ORIGINAL))

%__thumbnail-square.jpg: %.jpg
	convert  -geometry 800x800^ -gravity center -crop 800x800+0+0 "$<" "$@"

%__thumbnail-wide.jpg: %.jpg
	convert "$<" -resize "750>" "$@"

%.jpg: %.png
	convert "$<" "$@"

jpeg: $(JPEG_FROM_PNG) $(JPEG_SQUARE) $(JPEG_WIDE)

clean:
	rm -rf ${JPEG_FROM_PNG}
	find . -name '*thumbnail*' -delete

run:
	hugo server --buildDrafts -w --bind 0.0.0.0

build:
	docker build -t ${IMAGE} .

test: build
	@docker rm -f marketing || true
	docker run --name marketing -d -p 80:80 ${IMAGE}
	wget --no-verbose --recursive --spider http://localhost

docker-dev: build
	-docker rm -f marketing
	docker run --name marketing --rm -it -v $(shell pwd):/site --entrypoint=/bin/bash -p 80:80 -p 1313:1313 ${IMAGE}

