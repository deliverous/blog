---
title: Déploiement automatique depuis gitlab
tags:
  - docker
  - deliverous
  - continuous integration
  - continuous deployment
authors:
  - Thomas Clavier
description: Déploiement automatique depuis les pipelines gitlab sur deliverous
date: 2018-04-25
publishdate: 2018-04-25
draft: false
---

Dans cet article, je vais décrire pas à pas comment nous faisons pour déployer en continu sur deliverous depuis un dépôt gitlab en utilisant les pipelines de gitlab.

Les grandes étapes de notre pipeline sont les suivantes :

- construction du container
- lancement des tests
- déploiement sur deliverous


# Pré-requis

Pour avoir un pipeline de déploiement continu nous supposons que vous avez déjà:

- Un projet public sur un serveur gitlab accessible depuis internet par exemple [gitlab.com](http://gitlab.com)
- Un projet sur deliverous.com. Dans ce dernier, il est nécessaire d'avoir configuré l'url du projet gitlab, d'avoir un trigger et une IP.


# Construction

Dans le fichier `.gitlab-ci.yml` nous allons définir l'étape de build suivante :

    build:
      stage: build
      image: docker:latest
      services:
        - docker:dind
      script:
        - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
        - docker build -t $CI_REGISTRY_IMAGE .
        - docker push $CI_REGISTRY_IMAGE

Nous utilisons ici 3 variables automatiquement construites par gitlab :

- `CI_JOB_TOKEN` un token d'authentification sur la registry
- `CI_REGISTRY` l'url de la registry du gitlab
- `CI_REGISTRY_IMAGE` le nom du container par défaut, correspond à quelque chose du genre `registry.gitlab.com/<group>/<project>`

Cette étape construit et publie le container sur la registry interne du gitlab.

# Test

La seconde étape de notre pipeline d'intégration continue est la suivante :

    test:
      stage: test
      image: $CI_REGISTRY_IMAGE
      script:
        - mvn test

Nous utilisons à nouveau le nom du container par défaut, mais cette fois-ci comme base pour lancer nos tests. Vous aurez observé que dans notre exemple, nous avons un projet maven.


# Déploiement

Dernière étape, et pas des moindres, le déploiement. Elle permet de transformer notre pipeline d'intégration continue en chaîne de déploiement continu.

    deploy:
      stage: deploy
      script:
        - curl -X POST -data=""  https://deliverous.com/api/v1/trigger/$DELIVEROUS_TRIGGER

  Pour que cette dernière étape fonctionne, il est nécessaire de configurer la variable secrète `DELIVEROUS_TRIGGER` dans l'IHM de gitlab avec l'identifiant du trigger du projet deliverous.

  Comme décrit dans l'article sur [le fichier deliverous](/articles/2015-06-16_fichier-deliverous/), deliverous.com doit connaître le nom du container à déployer ainsi que l'IP à utiliser pour exposer le service. Il faut donc créer un fichier `Deliverous` à la racine de votre projet comme celui-ci :

    container1:
      image: registry.gitlab.com/<group>/<projet>
      ports:
      - ip: A.X.Y.Z
        container_port: 8080
        host_port: 80

Avec `registry.gitlab.com/<group>/<projet>` dans lequel vous aurrez pris soins de remplacer `registry.gitlab.com` par l'url de votre registry gitlab, `<group>` par le bon groupe gitlab et `<projet>` par le bon nom de projet. En effet, dans toutes les étapes précédentes, nous étions sur votre serveur gitlab et pas chez deliverous, avons pouvions donc utiliser la variable `CI_REGISTRY_IMAGE` pour nommer le container, ici il faut être explicite.

Enfin, n'oubliez pas de remplacer `A.X.Y.Z` par l'IP de votre projet sur deliverous.com.

# Pour finir

En conclusion, voici le déroulement global de l'opération :

1. Au push, gitlab va construire le container puis utiliser ce container pour lancer les tests avant de demander à deliverous de le lancer.
2. En s'appuyant sur le CI/CD et la registry Docker de gitlab il est relativement simple de partir du code source pour arriver à du déploiement continue sur deliverous. Ce blog utilise le même système de déploiement continu, vous pouvez d'ailleurs voir les fichiers [.gitlab-ci.yml](https://gitlab.com/deliverous/blog/blob/master/.gitlab-ci.yml) et [Deliverous](https://gitlab.com/deliverous/blog/blob/master/Deliverous)
